@javascript
Feature: Attach media directly to a content type
  In order to include resources with my content
  As an author
  I need to attach media to my content types
  
  @api
  Scenario: Newly uploaded image displays thumbnail 
    Given I am logged in as a user with the "administrator" role
    And I am on "/node/add/article"
    When I click "Select media"
    And I switch to the iframe "mediaBrowser"
    And I attach the file "koala.jpg" to "edit-upload"
    And I press "Submit"
    Then I should see the thumbnail
    And I should see "Remove media"

  @api
  Scenario: Image loads on full node view
    Given I am logged in as a user with the "administrator" role
    And I am on "/node/add/article"
    When I fill in "Title" with "Image Test"
    And I click "Select media"
    And I switch to the iframe "mediaBrowser"
    And I attach the file "koala.jpg" to "Upload a new file"
    And I press "Submit"
    And I press "Save"
    Then I should see the heading "Image Test"
    And the "koala.jpg" image should load

  @wip
  Scenario Outline: Displays show correct image size
    Given an article with an image attached
    When I view the "<display>"
    Then I should see the "<size>" image
    
    Examples:
    | display | size   |
    | node    | large  |
    | teaser  | medium |
    | list    | small  |
