This sandbox is for collaborative development of re-usable BDD steps for the Media module. 

Prerequisites
=============

1. A working local installation of Drupal 7

2. A Behat test suite configured to use the Drupal API for that local install
   http://dspeak.com/drupalextension/drupalapi.html

3. Your behat.yml file configured to discover SubContexts.
   See http://dspeak.com/drupalextension/subcontexts.html

Setup
=====

1. Clone this repository into sites/all/modules
   This will install Media and the behat tests, which are located in media/test/media.behat.inc

2. Copy the media/test/examples to your Behat features folder (or create a symlink)







